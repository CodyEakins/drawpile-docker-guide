# Drawpile - Docker Deployment Guide

> Drawpile is a open-source drawing program that allows artists to draw and 
paint together on the same digital canvas. To learn more about the program, or 
to download it, please visit the [official website](https://drawpile.net/).

In this guide, we're going to cover how to build and deploy a Drawpile server 
using [Docker](https://www.docker.com/).

## Table of Contents
- [Prerequisites](#prerequisites)
- [Get Started](#get-started)
- [Connecting to a Local Server](#connecting-to-a-local-server)
- [Hosting Sessions on a Local Server](#hosting-sessions-on-a-local-server)
- [Cleaning Up](#cleaning-up)
- [Afterword](#afterword)

## Prerequisites

This guide assumes the following prerequisites from its users:

1. You must have a working Docker installation on your computer.

   This guide **will not** cover how to install Docker. If you need to learn 
   how to install Docker, then please read the 
   [official documentation](https://docs.docker.com/v17.12/install/).

2. You must be familiar with your computer's native terminal.

3. You must have Drawpile installed on your computer (*Duh!*).

## Get Started

In order to deploy a Drawpile server, we must first download its official 
runtime image from [Docker Hub](https://hub.docker.com/r/callaa/drawpile-srv).

To do this, run the following command in your terminal:

```
docker pull callaa/drawpile-srv:2.1.11
```

> **Note:** At the time of this writing this guide, the newest version of 
`drawpile-srv` is version `2.1.11`. This is subject to change.

Once the image is downloaded, we can start a new container from it using the 
following command:

```
docker run -d -p 27750:27750 --name drawpile-srv callaa/drawpile-srv:2.1.11
```

This command does a few things:

1. It creates a new container (or "instance") of Drawpile's server runtime, 
`drawpile-srv`.

2. It uses the `-d` flag to run the container in "detached" mode.

   This means you do not have to keep your terminal open for 
   the server to continue running.

3. It binds the default port used by `drawpile-srv` (port `27750`) to your 
host computer's network interface. 

   This allows you to connect to server locally using Drawpile.

4. It uses the `--name` flag to give the container a custom name, 
`drawpile-srv`.

   This makes it easier to reference the container in later commands.

Congratulations! You should now have a basic Drawpile sever running 
locally on your computer.

To connect to your server, see the next section.

## Connecting to a Local Server

To connect to your local server, follow these steps:

1. Open Drawpile, then bring up the "join" menu using `Session > Join`: 

   !["Session > Join" Navigation](./img/session-join-navigation.png)

2. In the menu box that appears, use the input field at the bottom of the 
server list to connect to your server through `localhost`: 

   ![Join Menu](./img/join-menu.png)

3. After joining your server, you will be prompted to enter a username. Choose 
a username and continue:

   ![Username Prompt](./img/username-prompt.png)

4. Lastly, another menu box will appear which lists the server's currently 
active sessions:

   ![Session Menu](./img/session-menu.png)

Notice that the session list is empty. This is intentional!

You need to host a drawing session on the server before you can start using the 
canvas.

To host a session, see the next section.

## Hosting Sessions on a Local Server

To host a session on your local server, follow these steps:

1. Open Drawpile, then bring up the "host" menu using `Session > Host`:

   !["Session > Host" Navigation](./img/session-host-navigation.png)

2. In the menu box that appears, fill in your session details and continue.

   ![Host Menu](./img/host-menu.png)

   **Pay attention to the `server` section in this menu!**  
   Even though your server is hosted locally, you still need to use the 
   `remote` option to specify `localhost`.

Congratulations! You are now hosting a drawing session on your local server. 

If you open a second, separate Drawpile window, and connect to your server, you 
should see the session you just created in the server's session list.

> **Note:** This session will terminate automatically when all the users 
connected to it leave the session.

## Cleaning Up

In the [#get-started](#get-started) section, we created a new container for 
a local Drawpile server. 

This container will keep running until it is stopped or removed.

To stop the container, run the following command:
```
docker stop drawpile-srv
```

And to remove the stopped container, run this command:
```
docker rm drawpile-srv
```

## Afterword

In this guide, we learned how to deploy a basic Drawpile server using Docker.

This server was hosted locally, using the default settings that are baked into 
the official `drawpile-srv` image. These settings, however, may not be suitable 
for every server host or provider.

Fortunately, `drawpile-srv` is fully customizable. In fact, learning how to 
customize a Drawpile server will give you access to more options and features.

To learn how to customize a Drawpile server, read the 
[Advanced Deployment Guide]() (coming soon).

---

Copyright (c) 2019 - Calle Laakkonen, Drawpile 

**Written by:** Cody Eakins  
**Last Update:** August 14th, 2019